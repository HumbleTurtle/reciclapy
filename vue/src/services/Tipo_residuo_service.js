
import axios from 'axios'
import Config from './config'

import Tipo_residuo from '@/models/Tipo_residuo'

class Tipo_residuo_service {
  static loadList () {
    return axios.get('http://'+Config.server+'/tipos_residuos').then(function(response){

      var data = []
      for (let i = 0; i < response.data.length; i++) {
        data.push( new Tipo_residuo(response.data[i]) )
      }

      response.data = data
      return Promise.resolve(response)
    })
  }
}

export {Tipo_residuo_service as default}
