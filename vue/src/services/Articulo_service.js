
import axios from 'axios'
import Config from './config'

import Categoria from '@/models/Categoria'
import Articulo from '@/models/Articulo'

class Articulo_service {
  static get (data) {
    return axios.get('http://'+Config.server+'/articulos/'+data.id_articulo).then( function(response) {
      response.data = new Articulo(response.data)
      return Promise.resolve(response.data)
    } )
  }

}

export {Articulo_service as default}
