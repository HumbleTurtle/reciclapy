import axios from 'axios'
import config from './config'

const getCookie = (name) => {
  return document.cookie.split('; ').reduce((r, v) => {
    const parts = v.split('=')
    return parts[0] === name ? decodeURIComponent(parts[1]) : r
  }, '')
}

class Login_service {

  static check_login ( router ) {
    if(router.history.current.path == '/login' || router.history.current.path == '/registrarse')
      return true;

    if( getCookie('login') == "" ) {
      router.push('login')
      return false;
    }
  }

  static fakeLogin() {
    var d = new Date(); //

    d.setTime( d.getTime() + 3600 * 1000 ); // multiplicar por 1000 para convertir a time javascript
    var expires = "expires=" + d.toUTCString(); //Compose the expirartion date
    window.document.cookie = "login"+"="+"Hello World"+"; "+expires;//Set the cookie with value and the expiration date

  }

  static login ( email, password, store) {

      var formData = new FormData()
      formData.set('email', email)
      formData.set('password', password)

      /// Guarda el tiempo antes de enviar la llamada al api
      // Registra el tiempo de login y setea la expiracion de la cookie
      return axios.post('http://'+config.server+'/login', formData, {
        withCredentials: true
      }).then(function (response) {

        console.log(response.data)

        if ( response.data.error ) {
          console.log("rejected !")
          return Promise.reject( "Ha ocurrido un error.")
        }

         var d = new Date(); //

         d.setTime( parseInt( response.data.expires ) * 1000 ); // multiplicar por 1000 para convertir a time javascript
         var expires = "expires=" + d.toUTCString(); //Compose the expirartion date
         window.document.cookie = "login"+"="+"Hello World"+"; "+expires;//Set the cookie with value and the expiration date

        return Promise.resolve(response)
      }).catch( (err) => {
        console.log( err.response )
        if (err.response.status == 401)
          return Promise.reject("Datos erróneos.")
        return Promise.reject("Ha ocurrido un error inesperado.")
      })

  }

  static cerrar_sesion () {
    // Hacer post al api pidiendo eliminar la cookie de login httponly
    return axios.post('http://'+config.server+'/cerrar_sesion', {}, {withCredentials:true}).then( ()=>{
      // eliminar la cookie de control de login
      var d = new Date();

      d.setTime(d.getTime() - (1000*60*60*24));
      var expires = 'expires=' + d.toGMTString();

      window.document.cookie = 'authentication'+'='+'; +expires';

      return
    })
  }

  static registrarse (data) {
    var formData = new FormData()
    formData.set('username', data.username )
    formData.set('password', data.password )
    formData.set('email'   , data.email )

    return axios.post( 'http://'+config.server+'/registrarse', formData, {withCredentials:true} ).then( () => {
      return Promise.resolve(true)
    } ).catch( (error) => {

      if ( error.response.status == 409 ) {
        return Promise.reject("El email ya está registrado.")
      }
      return Promise.reject(error)
    } )

  }
}

export {Login_service as default}
