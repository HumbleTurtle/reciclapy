
import axios from 'axios'
import Config from './config'

import Residuo_reciclable from '@/models/Residuo_reciclable'

class Residuo_reciclable_service {

  static guardar (data) {

    var formData = new FormData()
    formData.set('ubicacion', JSON.stringify( data.ubicacion.toJSON() ) )
    formData.set('id_tipo_residuo', data.tipo_residuo.id_tipo_residuo )

    return axios.post('http://'+Config.server+'/residuos_reciclables', formData, {withCredentials:true})
  }

  static loadList (data) {
    return axios.get('http://'+Config.server+'/residuos_reciclables/page/'+data.page+'/limit/'+data.limit).then(function(response){

      var data = []
      for (let i = 0; i < response.data.length; i++) {
        data.push( new Residuo_reciclable(response.data[i]) )
      }

      response.data = data
      return Promise.resolve(response)
    })
  }
}

export {Residuo_reciclable_service as default}
