
import axios from 'axios'
import Config from './config'

import Categoria from '@/models/Categoria'
import Articulo from '@/models/Articulo'

class Categoria_service {
  static loadList () {
    return axios.get('http://'+Config.server+'/categorias').then(function(response){

      var data = []
      for (let i = 0; i < response.data.length; i++) {
        data.push( new Categoria(response.data[i]) )
      }

      response.data = data
      return Promise.resolve(response)
    })
  }

  static getArticulos ( data ) {
    return axios.get( 'http://'+Config.server+'/categorias/'+data.id_categoria+'/articulos' ).then( function(response){

      var data = []
      for (let i = 0; i < response.data.length; i++) {
        data.push( new Categoria( response.data[i]) )
      }

      response = data[0]
      return Promise.resolve(response)
    })
  }
}

export {Categoria_service as default}
