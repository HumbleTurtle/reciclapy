import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/login',
      component: () => import('./views/login/Login.vue')
    },
    {
      path: '/registrarse',
      component: () => import('./views/login/Registrarse.vue')
    },
    {
      path: '/reciclar',
      component: () => import('./views/reciclar/Reciclar.vue')
    },
    {
      path: '/denuncias',
      component: () => import('./views/denuncias/Denuncias.vue')
    },
    {
      path: '/mi-cuenta',
      component: () => import('./views/mi-cuenta/MiCuenta.vue')
    },
    {
      path: '/herramientas',
      component: () => import('./views/herramientas/Herramientas.vue')
    },
    {
      path:'/wiki',
      component: () => import('./views/wiki/Wiki.vue'),
      children: [
        {
          path: '/',
          component: () => import('./views/wiki/views/Default.vue')
        },
        {
          path: 'articulo/:id',
          component: () => import('./views/wiki/views/Articulo.vue')
        }
      ]
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
