
class Ubicacion {
  constructor ( ubicacion ) {

    if(ubicacion == null) {
      this._id_ubicacion = null
      this._longitud = null
      this._latitud = null
      return;
    }

    this._id_ubicacion = ubicacion.id_ubicacion
    this._longitud = ubicacion.longitud
    this._latitud = ubicacion.latitud
  }

  get longitud () {
    return this._longitud
  }
  get latitud () {
    return this._latitud
  }

  set LatLng(latlng) {
    this._longitud = latlng.lng
    this._latitud = latlng.lat
  }


  toJSON () {
    return {
      id_ubicacion: this._id_ubicacion,
      longitud: this._longitud,
      latitud: this._latitud
    }
  }

}

export {Ubicacion as default}
