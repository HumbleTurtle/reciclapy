
import Ubicacion from '@/models/Ubicacion'
import Tipo_residuo from '@/models/Tipo_residuo'

class Residuo_reciclable {
  constructor (data) {
    this._ubicacion = new Ubicacion(data.ubicacion)
    this._tipo_residuo = new Tipo_residuo(data.tipo_residuo)
    this._created_at = data.created_at
    this._updated_at = data.updated_at
  }

  get ubicacion() {
    return this._ubicacion
  }

  get tipo_residuo () {
    return this._tipo_residuo
  }

  get created_at () {
    return this._created_at
  }

  get updated_at () {
    return this._updated_at
  }

  toJSON () {
    return {
      tipo_residuo: this._tipo_residuo.toJSON(),
      ubicacion: this._ubicacion.toJSON()
    }
  }


}

export { Residuo_reciclable as default }
