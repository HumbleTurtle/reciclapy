
class Articulo {
  constructor (articulo) {
    this._id_articulo = articulo.id_articulo
    this._titulo = articulo.titulo
    this._contenido = articulo.contenido
    this._created_at = articulo.created_at
    this._updated_at = articulo.updated_at
    this._articulos_referencias = []

    if (articulo.referencias != null) {
      for (let i=0; i<articulo.referencias.length; i++) {
        this._articulos_referencias.push( new Articulo(articulo.referencias[i]) )
      }
    }

  }
  get id_articulo () {
    return this._id_articulo
  }
  
  get titulo () {
    return this._titulo
  }

  get contenido () {
    return this._contenido
  }

  get articulos_referencias() {
    return this._articulos_referencias
  }

  toJSON () {
    return {
      id_articulo: this._id_articulo,
      titulo: this._titulo,
      contenido: this._contenido,
      created_at: this._created_at,
      updated_at: this._updated_at
    }
  }

}

export {Articulo as default}
