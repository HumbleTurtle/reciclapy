
class Tipo_Residuo {
  constructor (tipo_residuo) {
    this._id_tipo_residuo = tipo_residuo.id_tipo_residuo
    this._nombre = tipo_residuo.nombre
    this._descripcion = tipo_residuo.descripcion
  }

  get id_tipo_residuo () {
    return this._id_tipo_residuo
  }

  get nombre () {
    return this._nombre
  }

  get descripcion () {
    return this._descripcion
  }

  toJSON () {
    return {
      id_tipo_residuo: this._id_tipo_residuo,
      nombre: this._nombre,
      descripcion: this._descripcion
    }
  }

}

export {Tipo_Residuo as default}
