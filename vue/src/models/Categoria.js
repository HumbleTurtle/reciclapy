import Articulo from '@/models/Articulo'
class Categoria {
  constructor (categoria) {
    this._id_categoria = categoria.id_categoria
    this._nombre = categoria.nombre
    this._articulos = []
    
    if( categoria.articulos !== null ) {

      for(let i = 0; i< categoria.articulos.length; i++) {
        this._articulos.push( new Articulo ( categoria.articulos[i] ) )
      }
    }
  }

  get nombre () {
    return this._nombre
  }

  get descripcion () {
    return this._id_categoria
  }

  get articulos () {
    return this._articulos
  }

  toJSON () {
    return {
      id_categoria: this._id_categoria,
      nombre: this._nombre,
      articulos: this._articulos
    }
  }

}

export {Categoria as default}
