import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import Home from "@/views/Home.vue"

export default new Vuex.Store({
  state: {
    platform_ios: false,
    navigation:[Home],
    navPages: [
      { name: "Mi cuenta", route: "/mi-cuenta" },
      { name: "Herramientas", route: "/herramientas"},
      { name: "Sobre nosotros", route: "/about"},
      { name: "Cerrar sesión", action: 'cerrar_sesion' }
    ],
    eventListeners: []
  },
  getters: {
    platform_ios: state => state.platform_ios,
    navigation: state => state.navigation,
    navPages: state => state.navPages
  },
  mutations: {
    addEventListener: (state,payload) => {
      state.eventListeners.push(payload)
    },
    emitEvent: (state,payload) => {
      for(let evento of state.eventListeners) {
        if(payload.dato !== null) {
          evento( payload.evento, payload.dato )
        } else {
          evento( payload.evento )
        }

      }
    },
    destroyEventListeners: (state) => {
      state.eventListeners = []
    },

    platform_ios: (state, payload) => { state.platform_ios = payload },
    navigationPush: (state, payload) => {
      state.navigation.push(payload)
    },
    navigationPop : (state) => {
      state.navigation.pop()
    }
  },
  actions: {

  }
})
