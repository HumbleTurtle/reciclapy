import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'

import router from './router'
import store from './store'

// (2) Webpack CSS import
import 'onsenui/css/onsenui.css';

import './assets/onsen-css-components.min.css'
import './assets/theme.css'

import "leaflet/dist/leaflet.css";

import L from 'leaflet';
delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

import 'ionicons/dist/css/ionicons.min.css'

import VueOnsen from 'vue-onsenui';

// (3) And plug in the bindings
Vue.use(VueOnsen);

axios.defaults.withCredentials = true
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
