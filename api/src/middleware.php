<?php
// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);


// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);
/*
  Falta aprender a leer de la cookie
*/

$app->add(new \Tuupola\Middleware\JwtAuthentication([
    "path" => "/", /* or ["/api", "/admin"] */
    "ignore" => ["/login","/registrarse"],
    "cookie" => "authentication",
    "secure" => false,
    "attribute" => "decoded_token_data",
    "secret" =>  $app->getContainer()->get("settings")['jwt']['secret'],
    "algorithm" => ["HS256"],
    "error" => function ($response, $arguments) {
        $data["status"] = "error";
        $data["message"] = $arguments["message"];
        return $response
            ->withHeader("Content-Type", "application/json")
            ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }
]));
