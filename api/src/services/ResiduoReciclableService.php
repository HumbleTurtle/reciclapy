<?php
namespace App\Services;

use App\Services\UbicacionService;
use App\Models\Residuo_reciclable;


class ResiduoReciclableService {

  public static function create ($data) {

    $residuoReciclable = new Residuo_reciclable();
    $residuoReciclable->id_ubicacion = $data->ubicacion->id_ubicacion;
    $residuoReciclable->id_tipo_residuo = $data->id_tipo_residuo;
    $residuoReciclable->id_usuario_hogar = $data->usuario_hogar->id_usuario_hogar;

    $residuoReciclable->save();
    return true;

  }

  public static function getList ( $id_usuario, $page, $limit ) {
    $residuosReciclables = Residuo_reciclable::where("id_usuario_hogar", $id_usuario)->take($limit)->skip(($page-1)*$limit)->orderBy('id_residuo_reciclable', 'DESC')->get();
    return $residuosReciclables;
  }

}
