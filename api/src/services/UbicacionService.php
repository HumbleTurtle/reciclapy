<?php
namespace App\Services;

use App\Models\Ubicacion;


class UbicacionService {
  public static function create($ubicacionParam) {
    $ubicacion = new Ubicacion();

    $ubicacion->longitud = $ubicacionParam->longitud;
    $ubicacion->latitud = $ubicacionParam->latitud;

    $ubicacion->save();
    return $ubicacion;
  }

  public static function createIfNExists ( $ubicacionParam ) {

    $ubicacion = Ubicacion::where('latitud', $ubicacionParam->latitud )->where('longitud', $ubicacionParam->longitud)->first();

    if( $ubicacion == null) {
      $ubicacion = self::create( $ubicacionParam );
    }
    return $ubicacion;
  }

  public static function getList ( $id_usuario, $page, $limit ) {
    // TODO
  }

}
