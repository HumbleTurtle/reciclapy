<?php
namespace App\Services;

use App\Models\Usuario;

class UsuarioService {
  public static function getAuthenticatedUser ( $email, $password ) {
    $user = Usuario::where("email", $email)->first();

    // verify email address.
    if ( $user === NULL) {
      return null;
    }

    // verify password.
    if ( !password_verify( $password, $user->password) ) {
      return null;
    }

    return $user;
  }

  public static function exists ($email) {
    $user = Usuario::where("email", $email)->first();
    if (!$user) {
      return false;
    }
    return true;
  }

  public static function create ($data) {

    $usuario = new Usuario();
    $usuario->username = $data->username;
    $usuario->password = password_hash( $data->password, PASSWORD_DEFAULT);
    $usuario->email = $data->email;

    $usuario->save();
    return true;

  }

}
