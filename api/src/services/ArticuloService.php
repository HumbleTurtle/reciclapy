<?php
namespace App\Services;

use App\Models\Articulo;

/*
  Tabla: Enlace
  ------------------------------
  id_enlace : BIGINT
  titulo : varchar(100)
  descripcion : varchar(2000)
  url : varchar(2083)
  image_url : varchar(2083)
  visibilidad : TINYINT
  id_grupo_enlace : BIGINT
  id_usuario : BIGINT
  created_at : DATETIME
  updated_at : DATETIME
*/

class ArticuloService {

  public static function get ( $articuloID ) {
    return Articulo::where('id_articulo',$articuloID)->with('Referencias')->first();
  }

}
