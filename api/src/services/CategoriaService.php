<?php
namespace App\Services;

use App\Models\Categoria;

/*
  Tabla: Enlace
  ------------------------------
  id_enlace : BIGINT
  titulo : varchar(100)
  descripcion : varchar(2000)
  url : varchar(2083)
  image_url : varchar(2083)
  visibilidad : TINYINT
  id_grupo_enlace : BIGINT
  id_usuario : BIGINT
  created_at : DATETIME
  updated_at : DATETIME
*/

class CategoriaService {

  public static function getList ( ) {
    $categorias = Categoria::all();
    return $categorias;
  }

  public static function getArticulos ($id_categoria) {
    return Categoria::where('id_categoria', $id_categoria )->with(
      ['articulos' => function($query) {
            $query->select(['id_articulo', 'titulo', 'id_categoria']); }]
            )->get();
  }

}
