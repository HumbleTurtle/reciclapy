<?php
namespace App\Services;

use App\Models\Tipo_residuo;

/*
  Tabla: Enlace
  ------------------------------
  id_enlace : BIGINT
  titulo : varchar(100)
  descripcion : varchar(2000)
  url : varchar(2083)
  image_url : varchar(2083)
  visibilidad : TINYINT
  id_grupo_enlace : BIGINT
  id_usuario : BIGINT
  created_at : DATETIME
  updated_at : DATETIME
*/

class TipoResiduoService {
  public static function getList ( ) {
    $tipos_residuos = Tipo_residuo::all();
    return $tipos_residuos;
  }
}
