<?php
namespace App\Services;
use Slim\Http\Request;

use \Firebase\JWT\JWT;

class TokenService{

  public static function getToken ($headers, $settings) {
    $encodedHeaders = [];
    foreach ($headers as $clave => $valor)
      $encodedHeaders[base64_encode($clave)] = base64_encode($valor);

    $body = $settings['jwt']['secret'];
    return JWT::encode($encodedHeaders, $body, "HS256" );
  }

  public static function getData (Request $request) {
    $tokenData = $request->getAttribute("decoded_token_data");
    $data = [];
    foreach ($tokenData as $clave => $valor)
      $data[base64_decode($clave)] = base64_decode($valor);
    return $data;
  }

}
