<?php
return [
    'settings' => [
        'displayErrorDetails' => false, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header
        'determineRouteBeforeAppMiddleware' => true,
        'db' => [
            'driver' => 'mysql',
            'host' => 'localhost',
            'database' => 'reciclapy',
            'username' => 'root',
            'password' => '',
            'charset'   => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix'    => '',
        ],
        'servers' => [
          'dest' => 'http://localhost:8080',
          'local'=> 'reciclapy.local'
        ],
        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
        // jwt settings
        "jwt" => [
            "secret" => "BzSQfNcc1BcnElkAkg-=1PzNfGhJJI¿afVRz"
        ],
    ],
];


/*
        'db' => [
            'driver' => 'mysql',
            'host' => 'localhost',
            'database' => 'reciclapy',
            'username' => 'root',
            'password' => '',
            'charset'   => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix'    => '',
        ],
        'servers' => [
          'dest' => 'http://localhost:8080',
          'local'=> 'reciclapy.local'
        ],

        'db' => [
            'driver' => 'mysql',
            'host' => 'localhost',
            'database' => 'wecodebe_reciclapy',
            'username' => 'wecodebe_repy',
            'password' => '-reciclapy-*Q',
            'charset'   => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix'    => ''
        ],
        'servers' => [
          'dest' => 'http://demo.reciclapy.com',
          'local'=> 'api.reciclapy.com'
        ],


*/
