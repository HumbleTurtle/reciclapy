<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Tipo_residuo extends Model {
  public $table = "Tipo_residuo";
  public $primaryKey = 'id_tipo_residuo';

  protected $guarded = [];

  public function Residuo_reciclable (){
    return $this->hasMany('App\Models\Residuo_reciclable');
  }
}
