<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Categoria extends Model {
  public $table = "Categoria";
  public $primaryKey = 'id_categoria';

  protected $guarded = [];

  public function Articulos (){
    return $this->hasMany('App\Models\Articulo','id_categoria');
  }

}
