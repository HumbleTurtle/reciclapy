<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Articulo extends Model {
  public $table = "Articulo";
  public $primaryKey = 'id_articulo';

  protected $hidden = [
      'id_administrador',
      'id_categoria',
      'pivot'
  ];

  public function Categoria(){
      return $this->belongsTo('App\Models\Categoria','id_categoria');
  }

  public function Referencias () {
    $r1 = $this->belongsToMany('App\Models\Articulo', 'Articulos_referencias', 'id_articulo', 'id_articulo_objetivo' );
    $r2 = $this->belongsToMany('App\Models\Articulo', 'Articulos_referencias', 'id_articulo_objetivo', 'id_articulo' );

    return $r2->union($r1);
  }

}
