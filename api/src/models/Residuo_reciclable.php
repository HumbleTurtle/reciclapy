<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Residuo_reciclable extends Model{
  public $table = "Residuo_reciclable";
  public $primaryKey = 'id_residuo_reciclable';

  protected $hidden = [
      'id_tipo_residuo',
      'id_ubicacion',
      'id_usuario_hogar',
      'id_usuario_reciclador'
  ];

  protected $with = [
    'tipo_residuo',
    'ubicacion'
  ];

  public function tipo_residuo(){
      return $this->belongsTo('App\Models\Tipo_residuo','id_tipo_residuo');
  }

  public function Ubicacion(){
      return $this->belongsTo('App\Models\Ubicacion','id_ubicacion');
  }
}
