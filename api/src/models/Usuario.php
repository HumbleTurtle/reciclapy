<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Usuario extends Model{
  protected $table = "Usuario";
  protected $primaryKey = 'id_usuario';
  public $timestamps = true;

}
