<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Usuario_hogar extends Model{
  protected $table = "Usuario_hogar";
  protected $primaryKey = 'id_usuario_hogar';
  public $timestamps = true;
}
