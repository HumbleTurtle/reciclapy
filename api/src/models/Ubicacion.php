<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Ubicacion extends Model{
  public $table = "Ubicacion";
  public $primaryKey = 'id_ubicacion';
  public $timestamps = false;
}
