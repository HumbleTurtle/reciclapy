<?php

namespace App\Controllers;

use App\Controllers\BaseController;

use App\Services\TokenService;
use App\Services\CategoriaService;

use Slim\Http\Request;
use Slim\Http\Response;


class CategoriaController extends BaseController{
  /*
    Tabla: Enlace
    ------------------------------
    id_enlace : BIGINT
    titulo : varchar(100)
    descripcion : varchar(2000)
    url : varchar(2083)
    image_url : varchar(2083)
    visibilidad : TINYINT
    id_grupo_enlace : BIGINT
    id_usuario : BIGINT
    created_at : DATETIME
    updated_at : DATETIME
  */
  private static function getArticulos ($response,$request,$args) {
    $categoriaID = $args["categoriaID"];

    $categorias = CategoriaService::getArticulos($categoriaID);
    return $response->withJson( $categorias );
  }

  private static function getList ($response,$request,$args) {
    $categorias = CategoriaService::getList();
    return $response->withJson( $categorias );
  }

  public static function registrar_rutas($app){

    $routeName = "categorias";

      $app->get( $routeName, function(Request $request, Response $response, array $args) {
        return self::getList($response,$request,$args);
      });

      $app->get( $routeName."/{categoriaID:[0-9]+}/articulos", function(Request $request, Response $response, array $args) {
        return self::getArticulos($response,$request,$args);
      });

  }

}
