<?php

namespace App\Controllers;

use App\Controllers\BaseController;

use App\Services\TokenService;

use App\Services\ResiduoReciclableService;
use App\Services\UbicacionService;

use Slim\Http\Request;
use Slim\Http\Response;

use App\Models\Residuo_reciclable;


class ResiduoReciclableController extends BaseController{
  /*
    Tabla: Enlace
    ------------------------------
    id_enlace : BIGINT
    titulo : varchar(100)
    descripcion : varchar(2000)
    url : varchar(2083)
    image_url : varchar(2083)
    visibilidad : TINYINT
    id_grupo_enlace : BIGINT
    id_usuario : BIGINT
    created_at : DATETIME
    updated_at : DATETIME
  */

  private static function create ($response,$request,$args) {

    $tokenData = TokenService::getData($request);
    $id_usuario = $tokenData["id_usuario"];

    $data = (object) array(
      "id_tipo_residuo" => $request->getParam('id_tipo_residuo'),
      "ubicacion" => (object) json_decode( $request->getParam('ubicacion'), true),
      "usuario_hogar" => (object) [ "id_usuario_hogar" => $id_usuario ]
    );

    $data->ubicacion = UbicacionService::createIfNExists( $data->ubicacion );

    $created = ResiduoReciclableService::create($data);

    if($created)
      return $response->withStatus(200);
    else
      return $response->withStatus(400);
  }

  private static function editar($response,$request,$args){

    $enlaceID = $args["id"];

    $modified = EnlaceService::modify($enlaceID, [
      "titulo" => $request->getParam('titulo'),
      "descripcion" => $request->getParam('descripcion')
    ]);

    // Actualmente solo se permite editar el titulo y la descripcion
    /*
    $enlace->url = $request->getParam('url');
    $enlace->image_url = $request->getParam('image_url');
    $enlace->visibilidad = $request->getParam('visibilidad');
    */
    if($modified){
      $response =  $response->withStatus(200);
    } else {
      $response =  $response->withStatus(404);
    }

    return $response;
  }

  private static function getList ($response,$request,$args) {
    $page = $args["page"];
    $limit = $args["limit"];

    $tokenData = TokenService::getData($request);
    $id_usuario = $tokenData["id_usuario"];

    $residuos = ResiduoReciclableService::getList($id_usuario, $page, $limit);

    return $response->withJson( $residuos );
  }

  public static function registrar_rutas($app){

    $routeName = "residuos_reciclables";
    // Básicos
          // Crear nuevo usuario
          $app->post($routeName,function(Request $request, Response $response, array $args){
            return self::create($response,$request,$args);
          });


          $app->get($routeName.'/page/{page:[0-9]+}/limit/{limit:[0-9]+}',function(Request $request, Response $response, array $args){
            return self::getList($response,$request,$args);
          });
/*
          // Obtener usuario específico
          $app->get("/".$routeName."/{id:[0-9]+}",function(Request $request, Response $response, array $args){
            return self::get($response,$request,$args);
          });



          $app->delete("/".$routeName."/{id:[0-9]+}",function(Request $request, Response $response, array $args){
            return self::remove($response,$request,$args);
          });
*/
  }

}
