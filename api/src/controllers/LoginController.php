<?php

namespace App\Controllers;

use App\Controllers\BaseController;

use App\Services\TokenService;
use App\Services\UsuarioService;

use Slim\Http\Request;
use Slim\Http\Response;

use App\Models\Usuario;

class LoginController extends BaseController{
  /*
    Tabla: Usuario
    id_usuario : int
    username : varchar (100)
    password : varchar (200)
    email : varchar(320)
    created_at : DATETIME
    updated_at :DATETIME
  */

  private static function check_login (Request $request, Response $response, array $args, $app) {
    $email = $request->getParam('email');
    $password =$request->getParam('password');

    $user = UsuarioService::getAuthenticatedUser($email, $password);

    // verify email address.
    if ($user === null) {
      return $response->withJson([ 'error'=>true, 'code' => 401, 'message' => 'These credentials do not match our records.'],401)->withStatus(401);
    }

    $settings = $app->get('settings'); // get settings array.

    $header = [
               'id_usuario' => $user->id_usuario,
               'email' =>$user->email,
               'date' => time(),
               'expires' => time()+3600
             ];

    $token = TokenService::getToken($header, $settings);
    setcookie("authentication", $token, time() + 3600, "/", $settings["servers"]["local"], 0, 1);

    $result = [
      "date" => $header["date"],
      "expires" => $header["expires"]
    ];

    return $response->withJson( $result );
  }

  private static function registrar (Request $request, Response $response, array $args, $app) {


    if ( UsuarioService::exists( $request->getParam('email') ) ) {
      return $response->withStatus(409)->withJson(['error' => true, 'message' => 'Email already exists.']);;
    }

    $data = (object) array(
      "username" => $request->getParam('username'),
      "password" => $request->getParam('password'),
      "email" => $request->getParam('email') ,
    );

    $created = UsuarioService::create( $data );


    if($created)
      return $response->withStatus(200);
    else
      return $response->withStatus(400);

  }

  private static function cerrar_sesion (Request $request, Response $response, array $args, $app) {

    if ( isset($_COOKIE['authentication']) ) {

        setcookie('authentication', null, time()-3600, '/', $app->get("settings")["servers"]["local"], 0, 1);
        unset($_COOKIE['authentication']);
    }

    return $response->withStatus(200);
  }

  public static function registrar_rutas ($app) {

    $app->post('/login', function (Request $request, Response $response, array $args) {
      return self::check_login($request, $response, $args, $this);
    });

    $app->post('/registrarse', function (Request $request, Response $response, array $args) {
      return self::registrar($request, $response, $args, $this);
    });

    $app->post('/cerrar_sesion', function (Request $request, Response $response, array $args) {
      return self::cerrar_sesion($request, $response, $args, $this);
    });

  }

}
