<?php

namespace App\Controllers;

use App\Controllers\BaseController;

use App\Services\TokenService;
use App\Services\ArticuloService;

use Slim\Http\Request;
use Slim\Http\Response;


class ArticuloController extends BaseController{
  /*
    Tabla: Enlace
    ------------------------------
    id_enlace : BIGINT
    titulo : varchar(100)
    descripcion : varchar(2000)
    url : varchar(2083)
    image_url : varchar(2083)
    visibilidad : TINYINT
    id_grupo_enlace : BIGINT
    id_usuario : BIGINT
    created_at : DATETIME
    updated_at : DATETIME
  */
  private static function get ($response,$request,$args) {
    $articuloID = intVal( $args["articuloID"] );

    $articulo = ArticuloService::get( $articuloID );
    return $response->withJson( $articulo );
  }

  public static function registrar_rutas($app){

    $routeName = "articulos";

      $app->get( $routeName."/{articuloID:[0-9]+}", function(Request $request, Response $response, array $args) {
        return self::get($response,$request,$args);
      });

  }

}
