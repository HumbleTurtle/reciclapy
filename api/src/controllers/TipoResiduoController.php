<?php

namespace App\Controllers;

use App\Controllers\BaseController;

use App\Services\TokenService;
use App\Services\TipoResiduoService;

use Slim\Http\Request;
use Slim\Http\Response;


class TipoResiduoController extends BaseController{
  /*
    Tabla: Enlace
    ------------------------------
    id_enlace : BIGINT
    titulo : varchar(100)
    descripcion : varchar(2000)
    url : varchar(2083)
    image_url : varchar(2083)
    visibilidad : TINYINT
    id_grupo_enlace : BIGINT
    id_usuario : BIGINT
    created_at : DATETIME
    updated_at : DATETIME
  */


  private static function create ($response,$request,$args) {

    $tokenData = TokenService::getData($request);

    $enlace = new Enlace();

    $data = [
      "titulo" => $request->getParam('titulo'),
      "descripcion" => $request->getParam('descripcion'),
      "url" => $request->getParam('url'),
      "image_url" => $request->getParam('image_url'),
      "visibilidad" => $request->getParam('visibilidad'),
      "id_usuario" => $tokenData["id_usuario"]
    ];

    if( !defined($request->getParam('id_grupo_enlace')) )
      $data["id_grupo_enlace"] = $request->getParam('id_grupo_enlace');


    $created = EnlaceService::newEnlace($data);

    if($created)
      return $response->withStatus(200);
    else
      return $response->withStatus(400);
  }

  private static function editar($response,$request,$args){

    $enlaceID = $args["id"];

    $modified = EnlaceService::modify($enlaceID, [
      "titulo" => $request->getParam('titulo'),
      "descripcion" => $request->getParam('descripcion')
    ]);

    // Actualmente solo se permite editar el titulo y la descripcion
    /*
    $enlace->url = $request->getParam('url');
    $enlace->image_url = $request->getParam('image_url');
    $enlace->visibilidad = $request->getParam('visibilidad');
    */
    if($modified){
      $response =  $response->withStatus(200);
    } else {
      $response =  $response->withStatus(404);
    }

    return $response;
  }

  private static function getList ($response,$request,$args) {
    $tiposResiduos = TipoResiduoService::getList();
    return $response->withJson( $tiposResiduos );
  }

  public static function registrar_rutas($app){

    $routeName = "tipos_residuos";

      $app->get($routeName,function(Request $request, Response $response, array $args){
        return self::getList($response,$request,$args);
      });

  }

}
