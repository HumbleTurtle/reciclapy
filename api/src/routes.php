<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require __DIR__ . '/../vendor/autoload.php';

use Slim\Http\Request;
use Slim\Http\Response;


use App\Controllers\LoginController;
use App\Controllers\ResiduoReciclableController;
use App\Controllers\TipoResiduoController;
use App\Controllers\CategoriaController;
use App\Controllers\ArticuloController;

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) use ($app) {
    $response = $next($req, $res);
    return $response
      ->withHeader('Access-Control-Allow-Origin', $this->get("settings")["servers"]["dest"] )
      ->withHeader('Access-Control-Allow-Credentials', "true" )
      ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
      ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

LoginController::registrar_rutas($app);

$app->group('/', function(\Slim\App $app) {
  ResiduoReciclableController::registrar_rutas($app);
  TipoResiduoController::registrar_rutas($app);
  CategoriaController::registrar_rutas($app);
  ArticuloController::registrar_rutas($app);
});

$app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:.+}', function($req, $res) {
    $handler = $this->notFoundHandler; // handle using the default Slim page not found handler
    return $handler($req, $res);
});
